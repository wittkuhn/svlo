## Description

This repository contains the "Snodgrass and Vanderwart 'Like' Objects".

> A new set of colored and shaded images commissioned by Bruno Rossion (Brown University and University of Louvain, Belgium) and Gilles Pourtois (Tilburg University, The Netherlands). Normative data similar to the original Snodgrass and Vanderwart paper (1980) have been collected. Each of these images was loosely based on an image from the original Snodgrass and Vanderwart set. Please cite the following paper in any presentation or published research using these new images: Rossion, B., & Pourtois, G. (2004). Revisiting Snodgrass and Vanderwart's object set: The role of surface detail in basic-level object recognition. Perception, 33, 217-236.

The can be downloaded for free e.g., [here](https://wiki.cnbc.cmu.edu/Objects).

All images either have size `281 x 197` (horizontal) or `197 x 281` (vertical).

Some necessary processing steps have been applied to the files before commiting them to this repository.
These steps are described in detail below:

## Initial conversion

### Download files

Download the `Snodgrass and Vanderwart 'Like' Objects (.zip)` from [here](https://wiki.cnbc.cmu.edu/Objects).

### Remove whitespace in filenames

Unfortunately, the filenames contain whitespace, e.g., `001 .PCT`.
To remove the whitespace from each filename, we ran this short bash code (taken from [here](https://gist.github.com/nepsilon/6896f1e0a075db59fb95#how-to-remove-spaces-in-filename-and-lowercasing)):

```bash
for oldname in *.PCT 
do 
  newname=`echo $oldname | sed -e 's/ //g'` 
  mv "$oldname" "$newname" 
done
```

### Convert `.pct` to `.jpg`

The original files are in the `.pct` format.
However, most experiment software cannot handle `.pct` files.
Therefore, we need to convert it into a commonly used format, like `.jpg`.

(Note, that previous conversions to `.png` or `.bmp` led to loss in some images)

To convert them, we can use the ImageMagick command-line function `convert` (see [here](https://imagemagick.org/script/convert.php)), for example:

```bash
convert 001.PCT 001.jpg
```

Now we are going to convert all files:

```bash
for file in *.pct ; do 
  filename="${file%.*}"
  convert $file ${filename}.jpg
done
```

You should briefly check if the converted files look fine.

### Remove `.pct` files

Finally, we want to remove the `.pct` file because we will not need them anymore. For example, we can use:

```bash
find . -type f -iname \*.pct -delete
```

Done! :tada:

## Requirements

* ImageMagick 7.0.7-22 Q16 x86_64 2018-01-13 http://www.imagemagick.org

## Maintainer

* [Lennart Wittkuhn](mailto:wittkuhn@mpib-berlin.mpg.de)

## License

TBA

## Reference

Please cite the following paper in any presentation or published research using these new images:

> Rossion, B., & Pourtois, G. (2004). Revisiting Snodgrass and Vanderwart's object set: The role of surface detail in basic-level object recognition. *Perception*, 33, 217-236.

```latex
@article{Rossion2004P,
	Abstract = {Theories of object recognition differ to the extent that they consider object representations as being mediated only by the shape of the object, or shape and surface details, if surface details are part of the representation. In particular, it has been suggested that color information may be helpful at recognizing objects only in very special cases, but not during basic-level object recognition in good viewing conditions. In this study, we collected normative data (naming agreement, familiarity, complexity, and imagery judgments) for Snodgrass and Vanderwart's object database of 260 black-and-white line drawings, and then compared the data to exactly the same shapes but with added gray-level texture and surface details (set 2), and color (set 3). Naming latencies were also recorded. Whereas the addition of texture and shading without color only slightly improved naming agreement scores for the objects, the addition of color information unambiguously improved naming accuracy and speeded correct response times. As shown in previous studies, the advantage provided by color was larger for objects with a diagnostic color, and structurally similar shapes, such as fruits and vegetables, but was also observed for man-made objects with and without a single diagnostic color. These observations show that basic-level `everyday' object recognition in normal conditions is facilitated by the presence of color information, and support a `shape + surface' model of object recognition, for which color is an integral part of the object representation. In addition, the new stimuli (sets 2 and 3) and the corresponding normative data provide valuable materials for a wide range of experimental and clinical studies of object recognition.},
	Author = {Rossion, Bruno and Pourtois, Gilles},
	Doi = {10.1068/p5117},
	Issn = {1468-4233},
	Journal = {Perception},
	Month = {Feb},
	Number = {2},
	Pages = {217--236},
	Publisher = {SAGE Publications},
	Title = {Revisiting {Snodgrass} and {Vanderwart's} Object Pictorial Set: The Role of Surface Detail in Basic-Level Object Recognition},
	Url = {http://dx.doi.org/10.1068/p5117},
	Volume = {33},
	Year = {2004}}
```

